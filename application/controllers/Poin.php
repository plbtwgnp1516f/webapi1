<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poin extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        $this->load->helper('url');
        /* ------------------ */

        $this->load->library('grocery_CRUD');

    }

    public function index()
    {
        $this->data();
    }

    public function data()
    {
        $this->grocery_crud->set_table('tbl_transaksi');
        $output = $this->grocery_crud->render();

        $this->_example_output($output);
    }

    function _example_output($output = null)
    {
        $this->load->view('poin_view.php',$output);
    }
}
