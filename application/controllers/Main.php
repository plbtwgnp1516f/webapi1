<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        $this->load->helper('url');
        /* ------------------ */

        $this->load->library('grocery_CRUD');

    }

    public function index()
    {
        $this->data();
    }

    public function data()
    {
        $this->grocery_crud->set_table('data');
        $output = $this->grocery_crud->render();

		    $state = $this->grocery_crud->getState();

		    if($state == 'add')
		    {
		        $output->css_files[] = base_url().'assets/infrastruktur/css/form.css';
		    }
		    elseif($state == 'edit')
		    {
		        $output->css_files[] = base_url().'assets/infrastruktur/css/form.css';
		    }

        $this->_example_output($output);
    }

    function _example_output($output = null)
    {
        $this->load->view('our_template.php',$output);
    }
}
