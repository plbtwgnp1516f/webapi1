<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {
	function __construct() {
		parent::__construct();
		//$this->load->model('login_model'); // Berfungsi untuk memanggil Login_model
		
		$this->load->library('session');
	}

	// Berfungsi untuk menampilkan halaman login
	function index() { 

		$data=array('title'=>'Login Administrator',
		'isi' =>'login_view');
		$this->load->view('login_view',$data);

		}

	// Berfungsi untuk melakukan validasi login
	function validasi() { 
		$data=array(
			'user_name'=>$this->input->post('user_name'),
			'password'=>md5($this->input->post('password'))
			);

		// Berfungsi untuk memanggil fungsi ambil_data pada class login_model
		$cek=$this->admin_login_model->ambil_data($data);
		$role=$this->admin_login_model->getRole($data['user_name'],$data['password']);
		// echo print_r($role);
		$sesi=$this->session->set_userdata($data);
		if($cek == 1) { // Berfungsi untuk mengecek kebenaran data login yang diinput (1 = true)
/*			if ($sesi['role']=="Admin") {
				
			// Jika data yang dimasukkan valid maka akan redirect ke halaman Dashboard
			redirect('admin/dashboard');
			} else {
				echo print_r("MASUK SEBAGAI USER");
			}

*/			


			// Berfungsi untuk menyimpan user data
			$sesi=$this->session->set_userdata($data);
			// Jika data yang dimasukkan valid maka akan redirect ke halaman Dashboard
			redirect('dashboard');

		}else{ // Jika data yang diinput tidak valid maka akan dialihkan ke view login gagal
			$this->load->view('login_gagal');
		}
		}
		

	// Berfungsi untuk menghapus session atau logout
	function logout() {
		session_destroy();
		redirect('admin/login');
		}	
}