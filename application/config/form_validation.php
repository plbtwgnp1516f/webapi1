<?php

defined('BASEPATH') OR exit('No direct script access allowed');


$config = array(
  'user_put'=>array(
    array('field'=>'email','label'=>'email','rules'=>'trim|required|valid_email'),
    array('field'=>'username','label'=>'username','rules'=>'trim|required|min_length[8]|max_length[16]'),
    array('field'=>'password','label'=>'password','rules'=>'trim|required|min_length[8]|max_length[16]'),
  ),
  'user_post'=>array(
    array('field'=>'email','label'=>'email','rules'=>'trim|valid_email'),
    array('field'=>'username','label'=>'username','rules'=>'trim|min_length[8]|max_length[16]'),
    array('field'=>'password','label'=>'password','rules'=>'trim|min_length[8]|max_length[16]'),
  ),
  'bencana_put'=>array(
    array('field'=>'id_laporan','field'=>'id_laporan','rules'=>'trim|required|numeric'),
    array('field'=>'waktu','field'=>'waktu','rules'=>'trim|required|numeric'),
    array('field'=>'username','field'=>'username','rules'=>'trim|required'),
    array('field'=>'penyebab','field'=>'penyebab','rules'=>'trim|required'),
    array('field'=>'korban','field'=>'korban','rules'=>'trim|required'),
    array('field'=>'total_kerugian','field'=>'total_kerugian','rules'=>'trim|required|numeric'),
    array('field'=>'tingkat_kerusakan','field'=>'tingkat_kerusakan','rules'=>'trim|required'),
	array('field'=>'jenis_bencana','field'=>'jenis_bencana','rules'=>'trim|required'),
	array('field'=>'alamat','field'=>'alamat','rules'=>'trim|required'),
	array('field'=>'kota','field'=>'kota','rules'=>'trim|required'),
	array('field'=>'negara','field'=>'negara','rules'=>'trim|required'),
	array('field'=>'desa','field'=>'desa','rules'=>'trim|required'),
	array('field'=>'kelurahan','field'=>'kelurahan','rules'=>'trim|required'),
	array('field'=>'kecamatan','field'=>'kecamatan','rules'=>'trim|required'),
  ),
  'bencana_post'=>array(
    array('field'=>'id_laporan','field'=>'id_laporan','rules'=>'trim|numeric'),
    array('field'=>'waktu','field'=>'waktu','rules'=>'trim|numeric'),
    array('field'=>'username','field'=>'username','rules'=>'trim'),
    array('field'=>'penyebab','field'=>'penyebab','rules'=>'trim'),
    array('field'=>'korban','field'=>'korban','rules'=>'trim'),
    array('field'=>'total_kerugian','field'=>'total_kerugian','rules'=>'trim|numeric'),
	array('field'=>'tingkat_kerusakan','field'=>'tingkat_kerusakan','rules'=>'trim'),
    array('field'=>'jenis_bencana','field'=>'jenis_bencana','rules'=>'trim'),
    array('field'=>'alamat','field'=>'alamat','rules'=>'trim'),
	array('field'=>'kota','field'=>'kota','rules'=>'trim'),
    array('field'=>'negara','field'=>'negara','rules'=>'trim'),
    array('field'=>'desa','field'=>'desa','rules'=>'trim'),
	array('field'=>'kelurahan','field'=>'kelurahan','rules'=>'trim'),
    array('field'=>'kecamatan','field'=>'kecamatan','rules'=>'trim'),
  ),
);

?>
