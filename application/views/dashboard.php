<?php
?>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Disaster</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url("assets/infrastruktur/css/bootstrap.min.css");?>" rel="stylesheet">

    <link href="<?php echo base_url("assets/infrastruktur/fonts/css/font-awesome.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/infrastruktur/css/animate.min.css");?>" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url("assets/infrastruktur/css/custom.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/infrastruktur/css/icheck/flat/green.css");?>" rel="stylesheet" />

    <script src="<?php echo base_url("assets/infrastruktur/js/jquery.min.js");?>"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><span>Disaster</span></a>
                    </div>
                    <div class="clearfix"></div>
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a href="<?php echo base_url("");?>"><i class="fa fa-home"></i> Beranda </a>
                                </li>
                                <li><a><i class="fa fa-edit"></i> Menu <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
										 <li><a href="<?php echo site_url("bencana");?>">Bencana</a>
                                        </li>
                                        <li><a href="<?php echo site_url("user");?>">User</a>
                                        </li>
                                        <li><a href="<?php echo site_url("poin");?>">Poin</a>
                                        </li>
                                        <li><a href="<?php echo site_url("reward");?>">Reward</a>
                                        </li>
										<li><a href="<?php echo site_url("Web_api");?>">Get Api</a>
                                        </li>
										<li><a href="<?php echo site_url("home");?>">Logout</a>
                                        </li>
										
                                    </ul>
                                </li>
                                <!-- <li><a><i class="fa fa-desktop"></i> Proses  <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="<?php echo base_url("presensi");?>">Presensi</a>
                                        </li>
                                        <li><a href="<?php echo base_url("pendaftaran");?>">Pendaftaran</a>
                                        </li>
                                        <li><a href="<?php echo base_url("bayar");?>">Pembayaran</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-table"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="<?php echo base_url("laporanAsli");?>">Laporan Pendapatan Asli</a>
                                        </li>
                                        <li><a href="<?php echo base_url("laporanPajak");?>">Laporan Pendapatan Pajak</a>
                                        </li>
                                        <li><a href="<?php echo base_url("laporanAnggota");?>">Laporan Anggota Aktif</a>
                                        </li>
                                    </ul>
                                </li> -->
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
            <div class="right_col" role="main">
                <!-- /top tiles -->
                <div class="row">

                </div>

                <!-- footer content -->

                
                <!-- /footer content -->
            </div>
            <!-- /page content -->

        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url("assets/infrastruktur/js/bootstrap.min.js");?>"></script>
    <!-- chart js -->
    <script src="<?php echo base_url("assets/infrastruktur/js/chartjs/chart.min.js");?>"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url("assets/infrastruktur/js/progressbar/bootstrap-progressbar.min.js");?>"></script>
    <script src="<?php echo base_url("assets/infrastruktur/js/nicescroll/jquery.nicescroll.min.js");?>"></script>
    <!-- icheck -->
    <script src="<?php echo base_url("assets/infrastruktur/js/icheck/icheck.min.js");?>"></script>

    <script src="<?php echo base_url("assets/infrastruktur/js/custom.js");?>"></script>
    <!-- /footer content -->
</body>
</html>
