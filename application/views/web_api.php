<?php
?>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Disaster</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url("assets/infrastruktur/css/bootstrap.min.css");?>" rel="stylesheet">

    <link href="<?php echo base_url("assets/infrastruktur/fonts/css/font-awesome.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/infrastruktur/css/animate.min.css");?>" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url("assets/infrastruktur/css/custom.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/infrastruktur/css/icheck/flat/green.css");?>" rel="stylesheet" />
	<link href="<?php echo base_url("assets/infrastruktur/css/form-element.css");?>" rel="stylesheet" />
    <script src="<?php echo base_url("assets/infrastruktur/js/jquery.min.js");?>"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><span>Disaster</span></a>
                    </div>
                    <div class="clearfix"></div>
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a href="home2"><i class="fa fa-home"></i> Beranda </a>
                                </li>
                                <li><a><i class="fa fa-edit"></i> Menu <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
										 <li><a href="<?php echo site_url("bencana");?>">Bencana</a>
                                        </li>
                                        <li><a href="<?php echo site_url("user");?>">User</a>
                                        </li>
                                        <li><a href="<?php echo site_url("poin");?>">Poin</a>
                                        </li>
                                        <li><a href="<?php echo site_url("reward");?>">Reward</a>
                                        </li>
										<li><a href="<?php echo site_url("Web_api");?>">Get Api</a>
                                        </li>
										<li><a href="<?php echo site_url("home");?>">Logout</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- <li><a><i class="fa fa-desktop"></i> Proses  <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="<?php echo base_url("presensi");?>">Presensi</a>
                                        </li>
                                        <li><a href="<?php echo base_url("pendaftaran");?>">Pendaftaran</a>
                                        </li>
                                        <li><a href="<?php echo base_url("bayar");?>">Pembayaran</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-table"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="<?php echo base_url("laporanAsli");?>">Laporan Pendapatan Asli</a>
                                        </li>
                                        <li><a href="<?php echo base_url("laporanPajak");?>">Laporan Pendapatan Pajak</a>
                                        </li>
                                        <li><a href="<?php echo base_url("laporanAnggota");?>">Laporan Anggota Aktif</a>
                                        </li>
                                    </ul>
                                </li> -->
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
            <div class="right_col" role="main">
                <!-- /top tiles -->
		<div class="row">
        <div class="col-md-12">
		<p>API bencana -> 
			<a href="http://andreasdimas-demo.esy.es/disaster/index.php/api//bencana/data">Rest API</a></p>
		
				
        {<br>
          <div class="a">
          "content": [<br>
          </div>
            <div class="b">
            {<br>
            </div>
                <div class = "c">
						{"status":"success","message"<br>
						:[{"id_laporan":"4",<br>
						"waktu":"2016-05-22 20:54:19",<br>
						"username":"admin",<br>
						"penyebab":"qew",<br>
						"korban":"231",<br>
						"status":"sudah dicek",<br>
						"total_kerugian":"0",<br>
						"tingkat_kerusakan":"Sedang",<br>
						"jenis_bencana":"Banjir",<br>
						"alamat":"",<br>
						"kota":"",<br>
						"negara":"",<br>
						"desa":"",<br>
						"kelurahan":"",<br>
						"kecamatan":""},<br>
						</div>
						 <div class="d">

						{"id_laporan":"5",<br>
						"waktu":"2016-05-30 22:03:40",<br>
						"username":"kevin",<br>
						"penyebab":"mboh",<br>
						"korban":"2",<br>
						"status":"tidak valid",<br>
						"total_kerugian":"0",<br>
						"tingkat_kerusakan":"Sedang",<br>
						"jenis_bencana":"Gempa Bumi",<br>
						"alamat":"Jalan Pangeran Diponegoro",<br>
						"kota":"Depok Sub-District",<br>
						"negara":"Indonesia",<br>
						"desa":"Depok Sub-District",<br>
						"kelurahan":"Maguwoharjo",<br>
						"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>

							{"id_laporan":"6",<br>
							"waktu":"2016-05-30 22:06:27",<br>
							"username":"kevin",<br>
							"penyebab":"kwkw",<br>
							"korban":"2",<br>
							"status":"tidak valid",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gempa Bumi",<br>
							"alamat":"Jalan Raya Pasekan",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"7",<br>
							"waktu":"2016-05-30 22:18:21",<br>
							"username":"kevin",<br>
							"penyebab":"ye",<br>
							"korban":"20",<br>
							"status":"tidak valid",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gempa Bumi",<br>
							"alamat":"Jalan Diponegoro",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"8",<br>
							"waktu":"2016-05-30 22:29:25",<br>
							"username":"kevin",<br>
							"penyebab":"re",<br>
							"korban":"5",<br>
							"status":"sudah dicek",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gunung Meletus",<br>
							"alamat":"Jalan Raya Pasekan",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
						
							{"id_laporan":"9",<div class = "c">
							{"status":"success","message"<br>
							:[{"id_laporan":"4",<br>
							"waktu":"2016-05-22 20:54:19",<br>
							"username":"admin",<br>
							"penyebab":"qew",<br>
							"korban":"231",<br>
							"status":"sudah dicek",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Banjir",<br>
							"alamat":"",<br>
							"kota":"",<br>
							"negara":"",<br>
							"desa":"",<br>
							"kelurahan":"",<br>
							"kecamatan":""},<br>
							</div>
							 <div class="d">
						</div>
						<div>
							{"id_laporan":"5",<br>
							"waktu":"2016-05-30 22:03:40",<br>
							"username":"kevin",<br>
							"penyebab":"mboh",<br>
							"korban":"2",<br>
							"status":"tidak valid",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gempa Bumi",<br>
							"alamat":"Jalan Pangeran Diponegoro",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
						
							{"id_laporan":"6",<br>
							"waktu":"2016-05-30 22:06:27",<br>
							"username":"kevin",<br>
							"penyebab":"kwkw",<br>
							"korban":"2",<br>
							"status":"tidak valid",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gempa Bumi",<br>
							"alamat":"Jalan Raya Pasekan",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
						
							{"id_laporan":"7",<br>
							"waktu":"2016-05-30 22:18:21",<br>
							"username":"kevin",<br>
							"penyebab":"ye",<br>
							"korban":"20",<br>
							"status":"tidak valid",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gempa Bumi",<br>
							"alamat":"Jalan Diponegoro",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"8",<br>
							"waktu":"2016-05-30 22:29:25",<br>
							"username":"kevin",<br>
							"penyebab":"re",<br>
							"korban":"5",<br>
							"status":"sudah dicek",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gunung Meletus",<br>
							"alamat":"Jalan Raya Pasekan",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"9",<br>
							"waktu":"2016-05-30 22:30:52",<br>
							"username":"kevin",<br>
							"penyebab":"re",<br>
							"korban":"5",<br>
							"status":"sudah dicek",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gunung Meletus",<br>
							"alamat":"Jalan Raya Pasekan",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"10",<br>
							"waktu":"2016-05-30 22:33:26",<br>
							"username":"kevin",<br>
							"penyebab":"tttt",<br>
							"korban":"20",<br>
							"status":"sudah dicek",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gempa Bumi",<br>
							"alamat":"Jalan Diponegoro",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"11",<br>
							"waktu":"2016-06-01 00:04:51",<br>
							"username":"kevin",<br>
							"penyebab":"kwkwkw",<br>
							"korban":"20",<br>
							"status":"tidak valid",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Pohon Tumbang",<br>
							"alamat":"Jalan Raya Tajem No.21",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
						
							{"id_laporan":"12",<br>
							"waktu":"2016-06-01 00:05:43",<br>
							"username":"kevin",<br>
							"penyebab":"kwkwkw",<br>
							"korban":"20",<br>
							"status":"sudah dicek",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Pohon Tumbang",<br>
							"alamat":"Jalan Raya Tajem No.21",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"13",<br>
							"waktu":"2016-06-01 01:53:28",<br>
							"username":"kevin",<br>
							"penyebab":"mboh",<br>
							"korban":"0",<br>
							"status":"sudah dicek",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Tinggi",<br>
							"jenis_bencana":"Pohon Tumbang",<br>
							"alamat":"Gang Garuda",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"14",<br>
							"waktu":"2016-06-01 01:56:30",<br>
							"username":"kevin",<br>
							"penyebab":"tsts",<br>
							"korban":"2",<br>
							"status":"sudah dicek",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gempa Bumi",<br>
							"alamat":"Gang Murai",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},{<br>
						</div>
						<div>
							"id_laporan":"15",<br>
							"waktu":"2016-06-01 01:56:50",<br>
							"username":"kevin",<br>
							"penyebab":"tsts",<br>
							"korban":"2",<br>
							"status":"sudah dicek",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gempa Bumi",<br>
							"alamat":"Gang Murai",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"16",<br>
							"waktu":"2016-06-01 09:44:20",<br>
							"username":"kevin",<br>
							"penyebab":"Hujan",<br>
							"korban":"999",<br>
							"status":"tidak valid",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Tinggi",<br>
							"jenis_bencana":"Hujan Es",<br>
							"alamat":"Jalan Babarsari No.21",<br>
							"kota":"",<br>
							"negara":"Indonesia",<br>
							"desa":"",<br>
							"kelurahan":"",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"17",<br>
							"waktu":"2016-06-01 14:12:33",<br>
							"username":"kevin",<br>
							"penyebab":"223",<br>
							"korban":"5",<br>
							"status":"sudah dicek",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gempa Bumi",<br>
							"alamat":"Jalan Tambak Bayan No.8",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Catur Tunggal",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"18",<br>
							"waktu":"2016-06-05 21:31:54",<br>
							"username":"kevin",<br>
							"penyebab":"123",<br>
							"korban":"222",<br>
							"status":"tidak valid",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Sedang",<br>
							"jenis_bencana":"Gempa Bumi",<br>
							"alamat":"Jalan Raya Pasekan",<br>
							"kota":"Depok Sub-District",<br>
							"negara":"Indonesia",<br>
							"desa":"Depok Sub-District",<br>
							"kelurahan":"Maguwoharjo",<br>
							"kecamatan":"Sleman Regency"},<br>
						</div>
						<div>
							{"id_laporan":"19",<br>
							"waktu":"2016-06-07 20:56:07",<br>
							"username":"au",<br>
							"penyebab":"lapar",<br>
							"korban":"2",<br>
							"status":"sudah dicek",<br>
							"total_kerugian":"0",<br>
							"tingkat_kerusakan":"Tinggi",<br>
							"jenis_bencana":"Kekeringan",<br>
							"alamat":"",<br>
							"kota":"",<br>
							"negara":"",<br>
							"desa":"",<br>
							"kelurahan":"",<br>
							"kecamatan":""}]}<br>
						</div>
      <div>
      </div><br><hr>
                </div>

                <!-- footer content -->

                
                <!-- /footer content -->
            </div>
            <!-- /page content -->

        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url("assets/infrastruktur/js/bootstrap.min.js");?>"></script>
    <!-- chart js -->
    <script src="<?php echo base_url("assets/infrastruktur/js/chartjs/chart.min.js");?>"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url("assets/infrastruktur/js/progressbar/bootstrap-progressbar.min.js");?>"></script>
    <script src="<?php echo base_url("assets/infrastruktur/js/nicescroll/jquery.nicescroll.min.js");?>"></script>
    <!-- icheck -->
    <script src="<?php echo base_url("assets/infrastruktur/js/icheck/icheck.min.js");?>"></script>

    <script src="<?php echo base_url("assets/infrastruktur/js/custom.js");?>"></script>
    <!-- /footer content -->
</body>
</html>
